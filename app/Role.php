<?php namespace App;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Trebol\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ['name', 'display_name', 'description'];

    // public function users() {
    //     return $this->hasMany(User::class, 'role_user');
    // }
    // public function users(){

    //     return $this->belongsToMany('App\User');
    // }
}
