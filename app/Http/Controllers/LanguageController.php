<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::first();
        Session::put('locale',$settings->language);
        app()->setLocale(Session::get('locale'));
        // Session::put('locale','hi');

        $languages = Language::get();
        return view('settings.index', compact('languages', 'settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('language.create');
        return redirect('/language');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = new Language();
        $languages->name = $request->name;
        $languages->code = $request->code;
        $languages->status = $request->status;
        $path = base_path() . '/resources/lang/' . $request->code;
        if (!file_exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $languages->save();
        return redirect('/language');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */

     public function show()
     {

     }
    public function edit($id)
    {
        $languages = Language::find($id);
        return view('language.edit' ,compact('languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request ,$id)
    {
        $languages = Language::find($id);
        $languages->name = $request->name;
        $languages->code = $request->code;
        $languages->status = $request->status;

        $languages->save();
        return redirect('/language');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $code = Language::select('code')->where('id',$id )->pluck('code');
        // Language::where('age', '29')->pluck('name');
        // dd($code[0]);
        $path = base_path().'/resources/lang/'.$code[0];
        // dd($path);
        if (file_exists($path)) {
              File::deleteDirectory($path);
          }
          Language::find($id)->delete();
        return redirect('/language');
    }

    public function settingUpdate(Request $request ,$id)
    {
        $settings = Setting::find($id);
        $settings->language = $request->language;

        $settings->save();
        return redirect()->route('settings.index');
    }
}
