<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::get();
        $projects = Project::get();
        return view('tasks.index' ,compact('tasks', 'projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     $projects = Project::get();
    //     return view('tasks.create' ,compact('projects'));
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $tasks = new Task();
        $tasks->title = $request->title;
        $tasks->project = $request->project;
        $tasks->start_date = $request->start_date;
        $tasks->dua_date = $request->dua_date;
        $tasks->assign_to = $request->assign_to;
        $tasks->description = $request->description;
        $tasks->save();
        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tasks = Task::find($id);
        $projects = Project::get();
        return view('tasks.edit' ,compact('tasks','projects'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        // dd($request->all());
        $tasks = Task::find($id);
        $tasks->title = $request->title;
        $tasks->project = $request->project;
        $tasks->start_date = $request->start_date;
        $tasks->dua_date = $request->dua_date;
        $tasks->assign_to = $request->assign_to;
        $tasks->description = $request->description;
        $tasks->save();
        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
