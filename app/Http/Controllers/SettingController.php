<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::first();
        return view('settings.index', compact('settings'));
    }

    public function create(){

    }
    public function store(Request $request){

    }
    public function show(){

    }

       public function edit($id)
    {
        $settings = Setting::find($id);
        return view('settings.index', compact('settings'));
    }


    public function update(Request $request ,$id)
    {
        $settings = Setting::find($id);
        $settings->language = $request->language;

        $settings->save();
        return redirect()->route('language.index');
    }



    public function destroy($id){

    }


}
