@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>projects</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('projects.index') }}">projects</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update Project</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('projects.update', $projects->id) }}" method="post" id="proUpdate">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter projects Name"
                                            name="name" autocomplete="off" value="{{ $projects->name }}">
                                        @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('start_date') has-error @enderror">
                                        <label for="start_date">Start Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" name="start_date"
                                            autocomplete="off" value="{{ $projects->start_date }}">
                                        @error('start_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('deadline') has-error @enderror">
                                        <label for="deadline">Deadline<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" name="deadline"
                                            autocomplete="off" value="{{ $projects->deadline }}">
                                        @error('deadline')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="project">Project Category</label>
                                        <select class="form-control" name="project_cate">
                                            <option value="1" {{ $projects->project_cate ==1 ? 'selected': '' }}>Laravel</option>
                                            <option value="2" {{ $projects->project_cate ==2 ? 'selected': '' }}>Yii</option>
                                            <option value="3" {{ $projects->project_cate ==3 ? 'selected': '' }}>Zend</option>
                                            <option value="4" {{ $projects->project_cate ==4 ? 'selected': '' }}>CatePhp</option>
                                            <option value="5" {{ $projects->project_cate ==5 ? 'selected': '' }}>Codeigniter</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="department">Department</label>
                                        <select class="form-control" name="department" id="department">
                                            <option value="1" {{ $projects->department ==1 ? 'selected': '' }} >Marketing</option>
                                            <option value="2" {{ $projects->department ==2 ? 'selected': '' }} >Sales</option>
                                            <option value="3" {{ $projects->department ==3 ? 'selected': '' }} >Human Resource</option>
                                            <option value="4" {{ $projects->department ==4 ? 'selected': '' }} >Public Relations</option>
                                            <option value="5" {{ $projects->department ==5 ? 'selected': '' }} >Research</option>
                                            <option value="6" {{ $projects->department ==6 ? 'selected': '' }} >Finance</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group @error('project_sum') has-error @enderror">
                                    <label for="project_sum">Project Summary<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="project_sum" id="project_sum" rows="3">
                                        {{ $projects->project_sum }}
                                    </textarea>
                                    @error('project_sum')
                                        <span class="text-danger"> {{ $message }} </span>
                                    @enderror
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <a href="{{ route('projects.index') }}" class="btn btn-secondary">Cancel</a>
                            {{-- <button type="submit" id="update_data" class="btn btn-primary">Update</button> --}}
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>


@endsection
