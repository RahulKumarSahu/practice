@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Project</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('projects.index') }}">Project</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <a href="{{ route('projects.create') }}" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Create</a> --}}
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Create
                            </button>
                        </div>

                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Start Date</th>
                                        <th>Dead-line</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @forelse ($projects as $project)
                                        <tr>
                                            <td>{{ $project->id }}</td>
                                            <td>{{ $project->name }}</td>
                                            <td>{{ $project->start_date }}</td>
                                            <td>{{ $project->deadline }}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('projects.show', $project->id) }}"
                                                        class="btn btn-warning">Show</a>
                                                    <div class="btn-group">
                                                        <a href="{{ route('projects.edit', $project->id) }}"
                                                            class="btn btn-primary">Update</a>
                                                        <form id="update" action="{{ route('projects.destroy', $project->id) }}"
                                                            method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit"
                                                                onclick="return confirm('Are you sure to detele')"
                                                                class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <td>
                                            <span class="badge badge-danger">No Projects</span>
                                        </td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Project</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="projectFrom" method="post">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control " id="name" placeholder="Enter Project Name"
                                            name="name" autocomplete="off">
                                        @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('start_date') has-error @enderror">
                                        <label for="date">Start Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                                        @error('start_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('deadline') has-error @enderror">
                                        <label for="date">Deadline<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" id="deadline" name="deadline" autocomplete="off">
                                        @error('deadline')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="project">Project Category</label>
                                        <select class="form-control" name="project_cate" id="project_cate">
                                            <option value="1">Laravel</option>
                                            <option value="2">Yii</option>
                                            <option value="3">Zend</option>
                                            <option value="4">CatePhp</option>
                                            <option value="5">Codeigniter</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="department">Department</label>
                                        <select class="form-control" name="department" id="department">
                                            <option value="1">Marketing</option>
                                            <option value="2">Sales</option>
                                            <option value="3">Human Resource</option>
                                            <option value="4">Public Relations</option>
                                            <option value="5">Research</option>
                                            <option value="6">Finance</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group @error('project_sum') has-error @enderror">
                                    <label for="project_sum">Project Summary<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="project_sum" id="project_sum" rows="3"></textarea>
                                    @error('project_sum')
                                        <span class="text-danger"> {{ $message }} </span>
                                    @enderror
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <button type="submit" class="btn btn-primary" id="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <script>
        $(document).ready(function(){

            $('#projectFrom').on('submit',function(e){
            e.preventDefault();
            // $('#submit').attr('disabled',true);

            let name = $('#name').val();
            let start_date = $('#start_date').val();
            let deadline = $('#deadline').val();
            let project_cate = $('#project_cate').val();
            let department = $('#department').val();
            let project_sum = $('#project_sum').val();

            $.ajax({
            url: "{{ route('projects.index') }}",
            type:"POST",
            data:{
                "_token": "{{ csrf_token() }}",
                name:name,
                start_date:start_date,
                deadline:deadline,
                project_cate:project_cate,
                department:department,
                project_sum:project_sum,
            },
            success:function(response){
                console.log(response);
                if (response) {
                $('#success-message').text(response.success);
                $("#projectFrom")[0].reset();

                $('#exampleModal').modal('hide');
                }
            },
            error: function(response) {
                console.log('error');
                // $('#name-error').text(response.responseJSON.errors.name);
                // $('#email-error').text(response.responseJSON.errors.email);
                // $('#mobile-number-error').text(response.responseJSON.errors.mobile_number);
                // $('#subject-error').text(response.responseJSON.errors.subject);
                // $('#message-error').text(response.responseJSON.errors.message);
            }
            });
            });
    });

    </script>

    @endsection
@endsection
