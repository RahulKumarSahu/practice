@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>projects</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('projects.index') }}">projects</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update Project</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form  method="post" id="proUpdate">
                        {{-- @csrf --}}
                        @method('PUT')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter projects Name"
                                            name="name"  value="{{ $projects->name }}" id="name">
                                        @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('start_date') has-error @enderror">
                                        <label for="start_date">Start Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" id="start_date" name="start_date"
                                             value="{{ $projects->start_date }}">
                                        @error('start_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('deadline') has-error @enderror">
                                        <label for="deadline">Deadline<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" name="deadline"
                                            id="deadline" value="{{ $projects->deadline }}">
                                        @error('deadline')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="project">Project Category</label>
                                        <select class="form-control" name="project_cate"  id="project_cate">
                                            <option value="1" {{ $projects->project_cate ==1 ? 'selected': '' }}>Laravel</option>
                                            <option value="2" {{ $projects->project_cate ==2 ? 'selected': '' }}>Yii</option>
                                            <option value="3" {{ $projects->project_cate ==3 ? 'selected': '' }}>Zend</option>
                                            <option value="4" {{ $projects->project_cate ==4 ? 'selected': '' }}>CatePhp</option>
                                            <option value="5" {{ $projects->project_cate ==5 ? 'selected': '' }}>Codeigniter</option>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="department">Department</label>
                                        <select class="form-control" name="department" id="department">
                                            <option value="1" {{ $projects->department ==1 ? 'selected': '' }} >Marketing</option>
                                            <option value="2" {{ $projects->department ==2 ? 'selected': '' }} >Sales</option>
                                            <option value="3" {{ $projects->department ==3 ? 'selected': '' }} >Human Resource</option>
                                            <option value="4" {{ $projects->department ==4 ? 'selected': '' }} >Public Relations</option>
                                            <option value="5" {{ $projects->department ==5 ? 'selected': '' }} >Research</option>
                                            <option value="6" {{ $projects->department ==6 ? 'selected': '' }} >Finance</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group @error('project_sum') has-error @enderror">
                                    <label for="project_sum">Project Summary<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="project_sum" id="project_sum" rows="3">
                                        {{ $projects->project_sum }}
                                    </textarea>
                                    @error('project_sum')
                                        <span class="text-danger"> {{ $message }} </span>
                                    @enderror
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <button type="button" id="update_data" value="{{ $projects->id}}" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

    @section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

    <script>
        $(document).ready(function(){

            $('#update_data').on('click',function(e){
            e.preventDefault();

            var name = $('#name').val();
            var start_date = $('#start_date').val();
            var deadline = $('#deadline').val();
            var project_cate = $('#project_cate').val();
            var department = $('#department').val();
            var project_sum = $('#project_sum').val();

            $.ajax({
            url: "{{ route('projects.update',$projects->id) }}",
            type:"POST",
            data:{
                "_token": "{{ csrf_token() }}",
                "_method": "PUT",
                name:name,
                start_date:start_date,
                deadline:deadline,
                project_cate:project_cate,
                department:department,
                project_sum:project_sum,
            },
            success:function(response){
                console.log(response);
                if (response) {
                $('#success-message').text(response.success);
                }
                window.location.href = "{{ route('projects.index') }}";
            },
            error: function(response) {
                console.log('error');
                // $('#name-error').text(response.responseJSON.errors.name);
                // $('#email-error').text(response.responseJSON.errors.email);
                // $('#mobile-number-error').text(response.responseJSON.errors.mobile_number);
                // $('#subject-error').text(response.responseJSON.errors.subject);
                // $('#message-error').text(response.responseJSON.errors.message);
            }
            });
            });
    });

    </script>

    @endsection


@endsection
