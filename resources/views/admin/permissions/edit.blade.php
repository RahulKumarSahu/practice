@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Role</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('permissions.index') }}">Role</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Roles</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('permissions.update', $permissions->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-body">

                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name">Name of Role<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Role" name="name"
                                    value="{{ $permissions->name }}">
                                @error('name')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>
                            <div class="form-group @error('display_name') has-error @enderror">
                                <label for="name">Display Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter display name"
                                    name="display_name" value="{{ $permissions->display_name }}">
                                @error('display_name')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('description') has-error @enderror">
                                <label for="description">Description<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="description" rows="3">
                                    {{ $permissions->description }}
                                </textarea>

                                    </textarea>
                                @error('description')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

@endsection
