@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Roles</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- @permission('role-create') --}}
                            {{-- @role('admin') --}}
                                <a href="{{ route('roles.create') }}" class="btn btn-primary">Create</a>
                            {{-- @endrole --}}
                            {{-- @endpermission --}}
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                Assign Role
                            </button>
                            {{-- <a href="{{ route('assingRole') }}" class="btn btn-primary">Assign Role</a> --}}
                        </div>

                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Display Name</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @forelse ($roles as $role)
                                        <tr>
                                            <td>{{ $role->id }}</td>
                                            <td>{{ $role->name }}</td>
                                            <td>{{ $role->display_name }}</td>
                                            <td>{{ $role->description }}</td>

                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('roles.edit', $role->id) }}"
                                                        class="btn btn-primary">Update</a>
                                                    <form action="{{ route('roles.destroy', $role->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit"
                                                            onclick="return confirm('Are you sure to detele')"
                                                            class="btn btn-danger">Delete</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <td>No Roles</td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Assign Role</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card-body">
                                        <table id="example2" class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Names</th>
                                                    <th>Roles</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($users as $user)
                                                    <tr>
                                                        {{-- <td>{{ $user->id }}</td> --}}
                                                        <td>{{ $user->name }}</td>
                                                        <td>
                                                            <select class="assignRole" data-user-id="{{$user->id}}">
                                                                @foreach ($roles as $role)
                                                                    <option value="{{ $role->id }}">{{ $role->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            $('.assignRole').change(function() {
                url = "{{ route('assignRole') }}";
                userId = $(this).data('user-id');
                roleId = $(this).val();
                // console.log(userId);

                $.ajax({
                    url: url,
                    type: 'GET',
                    data: {
                        'user_id': userId,
                        'role_id': roleId
                    },
                    success: function() {
                        swal("Created!", "Role has been Created.",
                            "success");
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                });
            });
        });
    </script>
@endsection
