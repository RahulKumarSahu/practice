@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Role</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Role</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Roles</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('roles.store') }}" method="post">
                        @csrf
                        <div class="card-body">

                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name">Name of Role<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Role" name="name">
                                @error('name')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>
                            <div class="form-group @error('display_name') has-error @enderror">
                                <label for="name">Display Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter display name" name="display_name">
                                @error('display_name')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('description') has-error @enderror">
                                <label for="description">Description<span class="text-danger">*</span></label>
                                <textarea class="form-control" name="description" rows="3"></textarea>

                                @error('description')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('description') has-error @enderror">
                                <label for="description">Permission</label>
                                <br>
                                @foreach ($permissions as $permission)
                                    <input class="form-check-input" type="checkbox" name="permission[]" value="{{ $permission->id }}"> {{ $permission->name }} <br>
                                @endforeach
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

@endsection
