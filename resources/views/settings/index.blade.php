@extends('layouts.app')


@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Settings</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Settings</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5 col-sm-3">
                                    <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                                        aria-orientation="vertical">

                                        <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill"
                                            href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home"
                                            aria-selected="true">General Setting</a>

                                        <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill"
                                            href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile"
                                            aria-selected="false">Language Setting</a>

                                    </div>
                                </div>

                                <div class="col-7 col-sm-9">
                                    <div class="tab-content" id="vert-tabs-tabContent">
                                        <div class="tab-pane fade active show" id="vert-tabs-home" role="tabpanel"
                                            aria-labelledby="vert-tabs-home-tab">

                                            <form action="{{ route('setting.update', $settings->id) }}" method="POST">
                                                @csrf
                                                @method('PUT')
                                                <div class="row">
                                                    <div class="col-sm-6">

                                                        <div class="form-group @error('language') has-error @enderror">
                                                            <label for="language">Language <span
                                                                    class="text-danger">*</span></label>
                                                            <select class="form-control" name="language" id="language">
                                                                @foreach ($languages as $language)
                                                                    <option value="{{ $language->code }}">
                                                                        {{ $language->name }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-6" style="margin-top: -61px;
                                                    margin-left: 819px;">
                                                        <button type="submit" class="btn btn-primary">Save</button>
                                                    </div>
                                                </div>

                                            </form>
                                            {{-- @include('settings.edit') --}}


                                        </div>

                                        <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel"
                                            aria-labelledby="vert-tabs-profile-tab">

                                            <div class="panel-body">
                                                {{-- <strong>Language</strong> --}}
                                                <div class="wrapper wrapper-content animated fadeInRight">
                                                    <span class="text-right" style="float: right; ">

                                                        <a href="{{ route('language.create') }}"
                                                            class="btn btn-primary"><span class="fa fa-plus"></span>Add
                                                            Language</a>

                                                    </span>
                                                    <span class="text-right"
                                                        style="float: right;margin:0px 20px 10px 0px;">

                                                        <a href="http://127.0.0.1:8000/translations"
                                                            class="btn btn-warning">
                                                            Translations</a>
                                                    </span>

                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Code</th>
                                                                <th>Status</th>
                                                                <th colspan="3">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($languages as $language)

                                                                <tr>
                                                                    <td>{{ $language->name }}</td>
                                                                    <td>{{ $language->code }}</td>
                                                                    <td>{{ $language->status }}</td>
                                                                    <td>
                                                                        <div class="btn-group">
                                                                            <a href="{{ route('language.update', $language->id) }}"
                                                                                class="btn-warning btn btn-xs">Edit</a>
                                                                            <form method="post"
                                                                                action="{{ route('language.destroy', $language->id) }}">
                                                                                @csrf
                                                                                @method('DELETE')
                                                                                <button
                                                                                    class="btn-danger btn btn-xs delete-user">Delete</button>
                                                                            </form>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div><!-- /.card-body -->

                    </div>


                </div>
            </div>
        </div>
    </section>

@endsection
