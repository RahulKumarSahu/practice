<!-- need to remove -->
<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link active">
        <i class="nav-icon fas fa-home"></i>
        <p>@lang('app.Home')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('user.index') }}" class="nav-link active">
        <i class="nav-icon fas fa-users"></i>
        <p>@lang('app.Users')</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('projects.index') }}" class="nav-link active">
        <i class="nav-icon fas fa-project-diagram"></i>
        <p>Projects</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('tasks.index') }}" class="nav-link active">
        <i class="nav-icon fas fa-project-diagram"></i>
        <p>Tasks</p>
    </a>
</li>
@role('super-admin')
<li class="nav-item">
    <a href="{{ route('permissions.index') }}" class="nav-link active">
        <i class="nav-icon fas fa-user"></i>
        <p>Permissions</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('roles.index') }}" class="nav-link active">
        <i class="nav-icon fas fa-user"></i>
        <p>Roles</p>
    </a>
</li>
@endrole
<li class="nav-item">
    <a href="{{ route('language.index') }}" class="nav-link active">
        <i class="nav-icon fas fa-cog"></i>
        <p>@lang('app.Settings')</p>
    </a>
</li>
