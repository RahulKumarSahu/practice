@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@lang('app.Language')</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('language.index') }}">@lang('app.Language')</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create User</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('language.store') }}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name">Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name">
                                @error('name')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('code') has-error @enderror">
                                <label for="code">Code<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="code" placeholder="code">
                                @error('code')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group"><label
                                class="col-sm-2 control-label">Select</label>
                            <div class="col-sm-12">
                                <select class="form-control" name="status"
                                    id="status">
                                    <option>Select</option>
                                    <option @if (old('status') == 'active') selected @endif
                                        value="active">Active
                                    </option>
                                    <option @if (old('status') == 'inactive') selected @endif
                                        value="inactive">Inactive
                                    </option>
                                </select>
                            </div>
                        </div>

                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="
                        text-align: center;">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

@endsection
