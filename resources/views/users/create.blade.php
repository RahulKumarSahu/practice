@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>@lang('app.Users')</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('user.index') }}">@lang('app.Users')</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create User</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('user.store') }}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name">Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Name" name="name">
                                @error('name')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('email') has-error @enderror">
                                <label for="exampleInputEmail1">Email<span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Enter email">
                                @error('email')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('password') has-error @enderror">
                                <label for="password">Password<span class="text-danger">*</span></label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                @error('password')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>


                            <div class="form-group @error('phone_number') has-error @enderror">
                                <label for="phone_number">phone_number</label>
                                <input type="number" class="form-control" name="phone_number" placeholder="Moblie No">
                            </div>
                            <div class="form-group @error('address') has-error @enderror">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="address" placeholder="address">

                            </div>

                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

@endsection
