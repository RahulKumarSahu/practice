@extends('layouts.app')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tasks</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('tasks.index') }}">Task</a></li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <a href="{{ route('projects.create') }}" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Create</a> --}}
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal1">
                                Create
                            </button>
                        </div>

                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Project</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @forelse ($tasks as $task)
                                        <tr>
                                            <td>{{ $task->id }}</td>
                                            <td>{{ $task->title }}</td>
                                            <td>{{ $task->project }}</td>
                                            <td>
                                                <div class="btn-group">

                                                    <div class="btn-group">
                                                        <a href="{{ route('tasks.edit', $task->id) }}"
                                                            class="btn btn-primary">Update</a>

                                                            <form action="{{ route('tasks.destroy', $task->id) }}"
                                                                method="post">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit"
                                                                    onclick="return confirm('Are you sure to detele')"
                                                                    class="btn btn-danger">Delete</button>
                                                            </form>
                                                    </div>
                                            </td>
                                        </tr>
                                    @empty
                                        <td>
                                            <span class="badge badge-danger">No Tasks</span>
                                        </td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="taskForm" method="post">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group @error('title') has-error @enderror">
                                        <label for="title">Title<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="title"
                                            placeholder="Enter Project Title" name="title" autocomplete="off">
                                        @error('title')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('start_date') has-error @enderror">
                                        <label for="date">Start Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" name="start_date" id="start_date"
                                            autocomplete="off">
                                        @error('start_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('dua_date') has-error @enderror">
                                        <label for="date">Due Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" name="dua_date" id="dua_date"
                                            autocomplete="off">
                                        @error('dua_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="assign_to">Assign To</label>
                                        <select class="form-control" name="assign_to" id="assign_to">
                                            <option value="1">Marketing</option>
                                            <option value="2">Sales</option>
                                            <option value="3">Human Resource</option>
                                            <option value="4">Public Relations</option>
                                            <option value="5">Research</option>
                                            <option value="6">Finance</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="project">Project Category</label>
                                        <select class="form-control" name="project" id="project">
                                            @foreach ($projects as $project )

                                            <option value="{{ $project->id}}">{{ $project->name}}</option>

                                            @endforeach
                                        </select>

                                    </div>

                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description<span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="description" id="description"
                                            rows="3"></textarea>
                                        @error('description')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>


                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </section>
    @section('js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

        <script>
            $(document).ready(function() {

                $('#taskForm').on('submit', function(e) {
                    e.preventDefault();

                    let title = $('#title').val();
                    let start_date = $('#start_date').val();
                    let dua_date = $('#dua_date').val();
                    let assign_to = $('#assign_to').val();
                    let project = $('#project').val();
                    let description = $('#description').val();

                    $.ajax({
                        url: "{{ route('tasks.index') }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            title: title,
                            project: project,
                            start_date: start_date,
                            dua_date: dua_date,
                            assign_to: assign_to,
                            description: description,
                        },
                        success: function(response) {
                            console.log(response);
                            if (response) {
                                $('#success-message').text(response.success);
                                $("#taskForm")[0].reset();
                                $('#exampleModal1').modal('hide');
                            }
                        },
                        error: function(response) {
                            console.log(' something missing ');
                            // $('#name-error').text(response.responseJSON.errors.name);
                            // $('#email-error').text(response.responseJSON.errors.email);
                            // $('#mobile-number-error').text(response.responseJSON.errors.mobile_number);
                            // $('#subject-error').text(response.responseJSON.errors.subject);
                            // $('#message-error').text(response.responseJSON.errors.message);
                        }
                    });
                });
            });
        </script>
    @endsection
@endsection
