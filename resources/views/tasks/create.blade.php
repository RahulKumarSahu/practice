@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Projects</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('tasks.index') }}">Projects</a></li>
                        <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Project</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('tasks.store') }}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group @error('name') has-error @enderror">
                                        <label for="name">Name<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter Project Name"
                                            name="name" autocomplete="off">
                                        @error('name')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('start_date') has-error @enderror">
                                        <label for="date">Start Date<span class="text-danger">*</span></label>
                                        <input type="date" class="form-control " name="start_date"
                                            autocomplete="off">
                                        @error('start_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('deadline') has-error @enderror">
                                        <label for="date">Deadline<span class="text-danger">*</span></label>
                                        <input type="date" class="form-control " name="deadline"
                                            autocomplete="off">
                                        @error('deadline')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="project">Project Category</label>
                                        <select class="form-control" name="project_cate">
                                            @foreach ($projects as $project )

                                            <option value="{{ $project-> name}}"></option>

                                            {{-- <option value="2">Yii</option>
                                            <option value="3">Zend</option>
                                            <option value="4">CatePhp</option>
                                            <option value="5">Codeigniter</option> --}}
                                            @endforeach
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <label for="department">Department</label>
                                        <select class="form-control" name="department">
                                            <option value="1">Marketing</option>
                                            <option value="2">Sales</option>
                                            <option value="3">Human Resource</option>
                                            <option value="4">Public Relations</option>
                                            <option value="5">Research</option>
                                            <option value="6">Finance</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="form-group @error('project_sum') has-error @enderror">
                                    <label for="project_sum">Project Summary<span class="text-danger">*</span></label>
                                    <textarea class="form-control" name="project_sum" rows="3"></textarea>
                                    @error('project_sum')
                                        <span class="text-danger"> {{ $message }} </span>
                                    @enderror
                                </div>
                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection
