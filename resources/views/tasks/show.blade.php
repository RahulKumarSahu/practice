@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>project</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('projects.index') }}">project</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Project</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="{{ route('projects.edit', $projects->id) }}" method="post">
                        @csrf
                        <div class="card-body">

                            <div class="form-group @error('name') has-error @enderror">
                                <label for="name">Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Project Name" name="name" value="{{ $projects->name }}">
                                @error('name')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>
                            <div class="form-group @error('progress') has-error @enderror">
                                <label for="progress">Progress <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Parcentage %" name="progress" value="{{ $projects->progress }}">
                                @error('progress')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('deadline') has-error @enderror">
                                <label for="deadline">Deadline<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="deadline" value="{{ $projects->deadline }}">
                                @error('deadline')
                                    <span class="text-danger"> {{ $message }} </span>
                                @enderror
                            </div>

                            <div class="form-group @error('status') has-error @enderror">
                                <label for="status">Status <span class="text-danger">*</span></label>
                                <select class="form-control" name="status" id="state">
                                    <option>Select</option>
                                    <option value="active"
                                        {{ $projects->status == 'active' ? 'selected' : '' }}>active</option>
                                    <option value="inactive"
                                        {{ $projects->status == 'inactive' ? 'selected' : '' }}>inactive</option>
                                </select>
                            </div>

                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <a href="{{ route('projects.index') }}" class="btn btn-warning">Cancel</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

@endsection
