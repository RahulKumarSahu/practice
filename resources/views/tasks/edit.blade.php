@extends('layouts.app')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tasks</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('projects.index') }}">Tasks</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Update</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form action="" method="post">

                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group @error('title') has-error @enderror">
                                        <label for="title">Title<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="title"
                                            placeholder="Enter Project Title" name="title" value="{{ $tasks->title }}">
                                        @error('title')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                    <div class="form-group @error('start_date') has-error @enderror">
                                        <label for="date">Start Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" name="start_date" id="start_date"
                                        value="{{ $tasks->start_date }}">
                                        @error('start_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                    <div class="form-group @error('dua_date') has-error @enderror">
                                        <label for="date">Due Date<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control datepicker" name="dua_date" id="dua_date"
                                        value="{{ $tasks->dua_date }}">
                                        @error('dua_date')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="assign_to">Assign To</label>
                                        <select class="form-control" name="assign_to" id="assign_to">
                                            <option value="1 {{ $tasks->assign_to ==1 ? 'selected': '' }} ">Marketing</option>
                                            <option value="2 {{ $tasks->assign_to ==2 ? 'selected': '' }} ">Sales</option>
                                            <option value="3 {{ $tasks->assign_to ==3 ? 'selected': '' }} ">Human Resource</option>
                                            <option value="4 {{ $tasks->assign_to ==4 ? 'selected': '' }} ">Public Relations</option>
                                            <option value="5 {{ $tasks->assign_to ==5 ? 'selected': '' }} ">Research</option>
                                            <option value="6 {{ $tasks->assign_to ==6 ? 'selected': '' }} ">Finance</option>
                                        </select>

                                    </div>

                                    <div class="form-group">
                                        <label for="project">Project Category</label>
                                        <select class="form-control" name="project" id="project">
                                            <option value="1 {{ $tasks->project ==1 ? 'selected': '' }} ">Laravel</option>
                                            <option value="2 {{ $tasks->project ==2 ? 'selected': '' }} ">Yii</option>
                                            <option value="3 {{ $tasks->project ==3 ? 'selected': '' }} ">Zend</option>
                                            <option value="4 {{ $tasks->project ==4 ? 'selected': '' }} ">CatePhp</option>
                                            <option value="5 {{ $tasks->project ==5 ? 'selected': '' }} ">Codeigniter</option>
                                        </select>

                                    </div>

                                    <div class="form-group @error('description') has-error @enderror">
                                        <label for="description">Description<span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="description" id="description"
                                            rows="3"> {{ $tasks->description }} </textarea>
                                        @error('description')
                                            <span class="text-danger"> {{ $message }} </span>
                                        @enderror
                                    </div>
                                </div>


                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer" style="text-align: center">
                            <button type="button" id="update_data" class="btn btn-primary">Update</button>
                        </div>
                    </form>

                </div>

            </div>
        </div>

    </div>
    @section('js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>

        <script>
            $(document).ready(function() {

                $('#update_data').on('click', function(e) {
                    e.preventDefault();

                    let title = $('#title').val();
                    let start_date = $('#start_date').val();
                    let dua_date = $('#dua_date').val();
                    let assign_to = $('#assign_to').val();
                    let project = $('#project').val();
                    let description = $('#description').val();

                    $.ajax({
                        url: "{{ route('tasks.update',$tasks->id) }}",
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "_token": "PUT",
                            title: title,
                            project: project,
                            start_date: start_date,
                            dua_date: dua_date,
                            assign_to: assign_to,
                            description: description,
                        },
                        success: function(response) {
                            console.log(response);
                            if (response) {
                                $('#success-message').text(response.success);
                                $("#taskForm")[0].reset();
                            }
                        },
                        error: function(response) {
                            console.log(' something missing ');
                            // $('#name-error').text(response.responseJSON.errors.name);
                            // $('#email-error').text(response.responseJSON.errors.email);
                            // $('#mobile-number-error').text(response.responseJSON.errors.mobile_number);
                            // $('#subject-error').text(response.responseJSON.errors.subject);
                            // $('#message-error').text(response.responseJSON.errors.message);
                        }
                    });
                });
            });
        </script>
    @endsection

@endsection
