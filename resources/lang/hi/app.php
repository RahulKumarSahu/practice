<?php

return array (
  'Hello' => 'नमस्ते',
  'Dashboard' =>'डैशबोर्ड',
  'Home' => 'घर',
  'Users' => 'उपयोगकर्ताओं',
  'Settings' => 'समायोजन',
  'Language' => 'भाषा',
);
