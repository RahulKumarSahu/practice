<?php

use App\Http\Controllers\LanguageController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/setLang',[LanguageController::class,'settingEdit'])->name('setLang');
Route::post('/setLang',[LanguageController::class,'settingUpdate'])->name('settingUpdate');

Route::get('/assignRole',[RoleController::class,'assignRoleToUser'])->name('assignRole');

Route::resource('user', UserController::class);
Route::resource('language', LanguageController::class);
Route::resource('setting', SettingController::class);
Route::resource('roles', RoleController::class);
Route::resource('permissions', PermissionController::class);
Route::resource('projects', ProjectController::class);
Route::resource('tasks', TaskController::class);
