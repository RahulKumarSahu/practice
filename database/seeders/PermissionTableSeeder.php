<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission=[

            [
                'name' => 'role-read',
                'display_name' => 'Display Role Listing',
                'description' => 'See only Listing of Role',
            ],
            [
                'name' => 'role-create',
                'display_name' => 'Create Role',
                'description' => 'Create New Role',
            ],
            [
                'name' => 'role-edit',
                'display_name' => 'Create Edit',
                'description' => 'Create New Edit',
            ],
            [
                'name' => 'role-delete',
                'display_name' => 'Create Delete',
                'description' => 'Create New Delete',
            ],
            [
                'name' => 'notebook-list',
                'display_name' => 'Display Notebook Listing',
                'description' => 'See only Listing of Notebook',
            ],
            [
                'name' => 'notebook-create',
                'display_name' => 'Create Notebook',
                'description' => 'Create  Notebook',
            ],
            [
                'name' => 'notebook-edit',
                'display_name' => 'Create Edit',
                'description' => 'Create New Edit',
            ],
            [
                'name' => 'notebook-delete',
                'display_name' => 'Notebook Delete',
                'description' => 'Notebook New Delete',
            ],
        ];

        foreach ($permission as $key=>$value)
        {
            Permission::create($value);
        }
    }
}
